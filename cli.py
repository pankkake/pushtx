#!/usr/bin/env python
import fileinput

from pushtx import pushtx

for transaction in fileinput.input():
    pushes = pushtx(transaction)
    for pusher, result in pushes.iteritems():
        print '%s: %s' % (pusher, 'Success' if result else 'Failure')
